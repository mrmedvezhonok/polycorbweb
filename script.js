let active_btn = undefined;
let roof_type = undefined;console.log('-_-_-_-_-_-_-_-_\n','Khitrin Sergey made this. If you see this message it means you are using demo version.\n','-_-_-_-_-_-_-_-_\n');

//Output
let table_leafs = $("#num_leafs"); //ячейка в таблице для вывода количества листво

// VALUES
// 1 unit = 1 mm;
const leaf_lenght = 12000; //постоянная величина длины листа
const leaf_width = 2100; //постоянная величина ширины листа
const roof_side = 10; // длина одного свеса листа

// other units
// сколько приходится на 1 сегмент: 
// const def_connector = 1; // соединителей (Соединительные профили)
const def_torcevoy = 2; // торцевых (профилей)
const def_termoshayba = 90; //термошайб
const def_samorez = 90; //саморезы
const def_perflenta = 2100; // перфолента (мм)
const def_germlenta = 2100;

// контейнеры с вводом информации
let len_input_container = $("#lenght-container"); // 0
let width_input_container = $("#width-container"); // 1
let height_input_container = $("#height-container"); // 2
let input_containers = [len_input_container, width_input_container, height_input_container];

// сами инпуты для ввода информации
let roof_len_input = $("#lenght-data"); // 0
let roof_width_input = $("#width-data"); // 1
let roof_height_input = $("#height-data"); // 2

let roof_data_inputs = [roof_len_input, roof_width_input, roof_height_input];

function activate_btn() {
    console.log('-_-_-_-_-_-_-_-_\n','Khitrin Sergey made this. If you see this message it means you are using demo version.\n','-_-_-_-_-_-_-_-_\n');
    if (active_btn) {
        active_btn.removeClass("active");
    }
    let current = $(this);
    current.addClass("active");
    active_btn = current;
    let calc_type = current.attr('data-roof');
    roof_type = +calc_type;

    switch(roof_type){
        case 1:
            show([0,1]);
            hide([2]);
            break;
        case 2:
            show([0,1, 2]);
            break;
        case 3:
            show([0,1, 2]);
            break;
    }
}

function show(elems){
    /*
        включает visibility: visible; для тех элементов, которые нужно отобразить (div)
        принимает массив elems, состоящий из чисел, где 0-длина,1-ширина, 2-высота
    */
    for(let i=0; i<elems.length; i++){
        input_containers[elems[i]].css("visibility", "visible");
    }
}

function hide(elems){
    /*
        включает visibility: hidden; для тех элементов, которые нужно скрыть (div)
        принимает массив elems, состоящий из чисел, где 0-длина,1-ширина, 2-высота
    */
    for(let i=0; i<elems.length; i++){
        input_containers[elems[i]].css("visibility", "hidden");
    }
}

function clear_form(){
    for(let i=0; i<roof_data_inputs.length; i++){
       roof_data_inputs[i].val(" ");
    }
}

function onCalc(){   
    console.log('-_-_-_-_-_-_-_-_\n','Khitrin Sergey made this. If you see this message it means you are using demo version.\n','-_-_-_-_-_-_-_-_\n');
    switch(roof_type){
        case 1: console.log('calc plane');
            calc_plane();
            break;
        case 2: console.log('calc arc');
            calc_arc();
            break;
        case 3: console.log('calc visor');
            calc_visor();
            break;
    }
}

function allowModal(flag=true, open=false){
    console.log('-_-_-_-_-_-_-_-_\n','Khitrin Sergey made this. If you see this message it means you are using demo version.\n','-_-_-_-_-_-_-_-_\n');
    if(flag){
        $("#btn-calc").attr('data-toggle', 'modal');
    }
    else{
        $("#btn-calc").attr('data-toggle', '');
    }
    if(open){
        $("#btn-calc").trigger('click');
    }
}

function calc_plane(){
  // Сначала проверяем, введены ли корректные данные в нужные поля для ввода
  let flag = false; //это для проверки, валидны ли нужные поля

  if(!roof_len_input[0].checkValidity()){
      roof_len_input.val(" ");
      flag = true;
  }
  if(!roof_width_input[0].checkValidity()){
      roof_width_input.val(" ");
      flag = true;
  }
  if (flag){
      allowModal(false, false); // если неверно введены данные в поля, то запрещаем модальному окну открываться
      return;
  }
  allowModal(); // если проверки пройдёны, то разрешаем модальному окну открываться

  // Рассчёт
  //длина и ширина листа
  let ll = leaf_lenght;
  let wl = leaf_width;

  // длина и ширина крыши (навеса), сразу переводим в мм из метров
  let lr = +roof_len_input.val() * 1000;
  let wr = +roof_width_input.val() * 1000;

  let rs = roof_side * 2; // 2 свеса 20 мм, каждый по 10 мм

  //Число сегментов из 1 листа.
  let num_segments_leaf = ll / (wr + rs);

  //Число сегментов, которое необходимо для покрытия всего навеса
  // let b = lr / wl;
  let num_segments = Math.ceil(lr / wl);
  // let num_segments = lr / wl;

  //Сколько нужно потратить листов
  let num_leafs_origin = num_segments / num_segments_leaf;
  let num_leafs = Math.ceil(num_leafs_origin);

  // let connectors = def_connector * (Math.ceil(wl/6000) + 1) * num_leafs;  // на каждые 6 метров 1 соединитель + 1, учитывая то что нужен как минимум 1 и на каждые следующие 6 метров новый
  let connectors = (wr>=6000?2:1) * num_segments; 
  let torcevoy = def_torcevoy * num_segments; 
  let termoshayba = def_termoshayba * num_segments;
  let samorez = def_samorez * num_segments;
  let perfolenta = 2*def_perflenta * num_segments;

  console.log(`calc plane, количество листов = ${num_leafs_origin.toFixed(2)}, округлённое число = ${num_leafs}, сегментов из 1 листа= ${num_segments_leaf.toFixed(3)}, всего сегментов = ${num_segments.toFixed(3)}`);

  out([["Листов поликарбоната", num_leafs], ["Соединительных профилей", connectors],
  ["Торцевых", torcevoy], ["Термошайб", termoshayba],
  ["Саморезов", samorez], ["Перфолента", perfolenta, 'мм.']], true);
}

function calc_arc(){
    console.log('-_-_-_-_-_-_-_-_\n','Khitrin Sergey made this. If you see this message it means you are using demo version.\n','-_-_-_-_-_-_-_-_\n');
  // Сначала проверяем, введены ли корректные данные в нужные поля для ввода
  let flag = false; //это для проверки, валидны ли нужные поля

  if(!roof_len_input[0].checkValidity()){
      roof_len_input.val(" ");
      flag = true;
  }
  if(!roof_width_input[0].checkValidity()){
      roof_width_input.val(" ");
      flag = true;
  }
  if(!roof_height_input[0].checkValidity()){
      roof_height_input.val(" ");
      flag = true;
  }
  if (flag){
      allowModal(false, false); // если неверно введены данные в поля, то запрещаем модальному окну открываться
      return;
  }
  allowModal(); // если проверки пройдёны, то разрешаем модальному окну открываться

  // Рассчёт
  //длина и ширина листа
  let ll = leaf_lenght;
  let wl = leaf_width;

  // длина, высота и ширина крыши (навеса), сразу переводим в мм из метров
  let lr = +roof_len_input.val() * 1000;
  let wr = +roof_width_input.val() * 1000;
  let hr = +roof_height_input.val() * 1000;

  // для арочного навеса предварительно необходимо найти длину дуги (L), которая будет шириной
  let c = wr/1000; //хорда
  let h = hr/1000; //высота
  let r = h/2 + (c*c)/(8*h); //радиус
  let al = 2*Math.asin(c/(2*r)); //угол альфа (радианы)
  // let ag = 180*al/Math.PI; //угол в градусах
  // let s = ((r*r)/2)*(al-Math.sin(al)); //площадь 
  let L = al*r* 1000;; //длина дуги в мм

  let rs = ([roof_side * 2]); // 2 свеса 20 мм, каждый по 10 мм
  //Число сегментов из 1 листа.
  let num_segments_leaf = ll / (L + rs);//вычисляем аналогично, но вместо ширины у нас длина дуги

  
  //Число сегментов, которое необходимо для покрытия всего навеса
  let num_segments = Math.ceil(lr / (wl));

  //Сколько нужно потратить листов
  let num_leafs_origin = num_segments / num_segments_leaf;
  let num_leafs = Math.ceil(num_leafs_origin);    
  
  let connectors = (wr>=6000?2:1) * num_segments; 
  let torcevoy = def_torcevoy * num_segments; 
  let termoshayba = def_termoshayba * num_segments;
  let samorez = def_samorez * num_segments;
  let perfolenta = 2*def_perflenta * num_segments;

  console.log(`c=${c}, h=${h}, r=${r}, a=${al}, L=${L}`);
  console.log(`calc plane, количество листов = ${num_leafs_origin.toFixed(2)}, округлённое число = ${num_leafs}, сегментов из 1 листа= ${num_segments_leaf.toFixed(3)}, всего сегментов = ${num_segments.toFixed(3)}`);

  out([["Листов поликарбоната", num_leafs], ["Соединительных профилей", connectors],
  ["Торцевых", torcevoy], ["Термошайб", termoshayba],
  ["Саморезов", samorez], ["Перфолента", perfolenta, 'мм.']], true);
}

function calc_visor(){ //козырёк
  // Сначала проверяем, введены ли корректные данные в нужные поля для ввода
  let flag = false; //это для проверки, валидны ли нужные поля

  if(!roof_len_input[0].checkValidity()){
      roof_len_input.val(" ");
      flag = true;
  }
  if(!roof_width_input[0].checkValidity()){
      roof_width_input.val(" ");
      flag = true;
  }
  if(!roof_height_input[0].checkValidity()){
      roof_height_input.val(" ");
      flag = true;
  }
  if (flag){
      allowModal(false, false); // если неверно введены данные в поля, то запрещаем модальному окну открываться
      return;
  }
  allowModal(); // если проверки пройдёны, то разрешаем модальному окну открываться

  // Рассчёт
  //длина и ширина листа
  let ll = leaf_lenght;
  let wl = leaf_width;

  // длина, высота и ширина крыши (навеса), сразу переводим в мм из метров
  let lr = +roof_len_input.val() * 1000;
  let wr = +roof_width_input.val() * 1000;
  let hr = +roof_height_input.val() * 1000;

  let rs = roof_side * 2; // 2 свеса 20 мм, каждый по 10 мм

  // для арочного навеса предварительно необходимо найти длину дуги (L), которая будет шириной
  let c = wr/1000; //хорда
  let h = hr/1000; //высота
  let r = h/2 + (c*c)/(8*h); //радиус
  let al = 2*Math.asin(c/(2*r)); //угол альфа (радианы)
  // let ag = 180*al/Math.PI; //угол в градусах
  // let s = ((r*r)/2)*(al-Math.sin(al)); //площадь 
  let L = al*r* 1000;; //длина дуги в мм

  //Число сегментов из 1 листа.
  let num_segments_leaf = ll / (L + rs);//вычисляем аналогично, но вместо ширины у нас длина дуги

  //Число сегментов, которое необходимо для покрытия всего навеса
  let num_segments = Math.ceil(lr / wl);

  //Сколько нужно потратить листов
  let num_leafs_origin = num_segments / num_segments_leaf;
  let num_leafs = Math.ceil(num_leafs_origin);

  let connectors = (wr>=6000?2:1) * num_segments; 
  let torcevoy = def_torcevoy * num_segments / 2; 
  let termoshayba = def_termoshayba * num_segments; 
  let samorez = def_samorez * num_segments; 
  let perfolenta = def_perflenta * num_segments;
  let germlenta = def_germlenta * num_segments;

  console.log(`c=${c}, h=${h}, r=${r}, a=${al}, L=${L}`);
  console.log(`calc plane, количество листов = ${num_leafs_origin.toFixed(2)}, округлённое число = ${num_leafs}, сегментов из 1 листа= ${num_segments_leaf.toFixed(3)}, всего сегментов = ${num_segments.toFixed(3)}`);

  out([["Листов поликарбоната", num_leafs], ["Соединительных профилей", connectors],
  ["Торцевых", torcevoy], ["Термошайб", termoshayba],
  ["Саморезов", samorez], ["Перфолента", perfolenta, 'мм.'], ["Гермитизирующая лента", germlenta, 'мм.']], true);
}
function out(elems, clear=true){
    console.log('-_-_-_-_-_-_-_-_\n','Khitrin Sergey made this. If you see this message it means you are using demo version.\n','-_-_-_-_-_-_-_-_\n');
  clear==true && $("#result-table-body").empty();
  for(let e of elems){
      $("#result-table-body").append(
          `<tr>
              <th scope="col">${e[0]}</th>
              <td>${e[1]} ${(e[2]==undefined?'шт.':e[2])}</td> 
          </tr>`);
  }
}